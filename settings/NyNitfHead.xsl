<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
	xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"

>

<!--
<xsl:output method="xml" encoding="utf-8" standalone="yes" indent="yes"/>
-->

<!-- 
	Styleshet for transformasjon fra NTB internt XML-format til NITF
	Felles mal for NTIF-Header
	Opprettet Av Roar Vestre 27.11.2001
	Sist endret Av Roar Vestre 29.11.2002
	Lagt til "meta"-tagger
	
	NB! M� endres for � passe med nytt Word/Notabene format
	Slik at alle NITF tagger blir mappet mot tilsvarende meta tagger 
	i Word/notabene xml-formatet
-->

<xsl:template match="/">
	<xsl:apply-templates select="/w:wordDocument/head"/>
</xsl:template>

<xsl:variable name="timestamp" select="//head/meta[@name='timestamp']/@content"/>
<xsl:variable name="kul" select="//head/nitf-elem/tobject.subject[@tobject.subject.code = 'KUL']"/>
<xsl:variable name="tema" select="//head/meta[@name='NTBTjeneste' and @content = 'NTBTema']"/>

<xsl:variable name="date-no">
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text> </xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 15, 2)" />
</xsl:variable>

<xsl:variable name="date-iso">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
	<xsl:value-of select="substring($timestamp, 6, 2)" />
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
	<xsl:value-of select="substring($timestamp, 15, 2)" />
	<xsl:value-of select="substring($timestamp, 18, 2)" />
<!--	<xsl:text>+0100</xsl:text>-->
</xsl:variable>

<xsl:variable name="date-iso-norm">
	<xsl:value-of select="substring($timestamp, 1, 4)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 6, 2)" />
		<xsl:text>-</xsl:text>
	<xsl:value-of select="substring($timestamp, 9, 2)" />
		<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($timestamp, 12, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 15, 2)" />
		<xsl:text>:</xsl:text>
	<xsl:value-of select="substring($timestamp, 18, 2)" />
<!--	<xsl:text>+01:00</xsl:text>-->
</xsl:variable>

<xsl:variable name='infolinje'>
	<xsl:choose>
	<xsl:when test="/w:wordDocument/w:body/wx:sect/w:p[w:pPr/w:pStyle/@w:val='Infolinje' and position() = 1]">
		<xsl:value-of select="/w:wordDocument/w:body/wx:sect/w:p[w:pPr/w:pStyle/@w:val='Infolinje' and position() = 1]"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:choose>
			<xsl:when test="/w:wordDocument/w:body/wx:sect/w:p/w:r/w:t = ''">
				<xsl:value-of select="substring(/w:wordDocument/w:body/wx:sect, 1, 60)" />
			</xsl:when>
			<xsl:when test="string-length(/w:wordDocument/w:body/wx:sect/w:p/w:r/w:t) &lt; 60">
				<xsl:value-of select="/w:wordDocument/w:body/wx:sect/w:p/w:r/w:t" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring(/w:wordDocument/w:body/wx:sect/w:p/w:r/w:t, 1, 60)" />
				<xsl:text>.....</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<!-- Main Template for NITF-header -->
<xsl:template match="head">

<head>
<!--
<title><xsl:value-of select="meta[@name='NTBStikkord']/@content"/></title>
<title><xsl:value-of select="/w:wordDocument/w:body/wx:sect/w:p[w:pPr/w:pStyle/@w:val='Infolinje']" /></title>
-->
<title><xsl:value-of select="$infolinje"/></title>

<xsl:for-each select="meta[@name='timestamp' 
	or @name='foldername' 
	or @name='filename' 
	or @name='subject' 
	or @name='NTBTjeneste' 
	or @name='NTBStikkord' 
	or @name='SMSTextFelt'
	or @name='NTBUtDato'
	or @name='NTBMeldingsType'
	or @name='NTBDistribusjonsKode'
	or @name='NTBSendTilDirekte'
	or @name='NTBSendTilDirektePluss'
	or @name='NTBTicker'
	or @name='NTBLangDirekte'
	or @name='NTBKanal'
	or @name='NTBID'
	or @name='NTBIPTCSequence'
	or @name='NTBSendTilNorden'
	or @name='NTBBilderAntall'
	or @name='NTBMeldingsSign'
	or @name='NTBPrioritet'
	or @name='ntb-lyd'
	or @name='Multimediefil'
	]">
<!--
<xsl:for-each select="meta">
-->	<meta>
		<xsl:attribute name="name">
			<xsl:value-of select="@name"/>
		</xsl:attribute>
		<xsl:attribute name="content">
			<xsl:value-of select="@content"/>
		</xsl:attribute>
	</meta>
</xsl:for-each>

<!--
<xsl:copy-of select="meta[@name='timestamp'] | meta[@name='foldername']"/>
<xsl:copy-of select="meta[@name='filename' or @name='madebydistributor' or @name='SMSTextFelt']"/>
-->

<meta name='ntb-dato'><xsl:attribute name="content"><xsl:value-of select="$date-no"/></xsl:attribute></meta>

<tobject>
	<xsl:attribute name="tobject.type"><xsl:value-of select="meta[@name='NTBStoffgruppe']/@content"/></xsl:attribute>
	<tobject.property>
		<xsl:attribute name="tobject.property.type"><xsl:value-of select="meta[@name='NTBUndergruppe']/@content"/></xsl:attribute>
	</tobject.property>
	<xsl:for-each select="nitf-elem/tobject.subject">
		<tobject.subject>
			<xsl:attribute name="tobject.subject.code">
				<xsl:value-of select="@tobject.subject.code"/>
			</xsl:attribute>
			<xsl:attribute name="tobject.subject.refnum">
				<xsl:value-of select="@tobject.subject.refnum"/>
			</xsl:attribute>
			<xsl:if test="@tobject.subject.type">
				<xsl:attribute name="tobject.subject.type">
					<xsl:value-of select="@tobject.subject.type"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@tobject.subject.matter">
				<xsl:attribute name="tobject.subject.matter">
					<xsl:value-of select="@tobject.subject.matter"/>
				</xsl:attribute>
			</xsl:if>
		</tobject.subject>
	</xsl:for-each>

<!--
	<xsl:copy-of select="nitf/tobject.subject"/>
-->
</tobject>


<docdata>
<!--
	<xsl:copy-of select="nitf/evloc"/>
-->
<xsl:for-each select="nitf-elem/evloc">
	<evloc>
		<xsl:attribute name="state-prov"><xsl:value-of select="@state-prov"/></xsl:attribute>
		<xsl:if test="@county-dist">
			<xsl:attribute name="county-dist"><xsl:value-of select="@county-dist"/></xsl:attribute>
		</xsl:if>
	</evloc>
</xsl:for-each>

	<doc-id>
		<xsl:attribute name="regsrc">
			<xsl:choose>
				<xsl:when test="/w:wordDocument/head/meta[@name='NTBTjeneste' and (@content='NPKTjenesten' or @content='NPKDirektetjenesten')]">
					<xsl:text>NPK</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>NTB</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
		<xsl:attribute name="id-string"><xsl:value-of select="meta[@name='ntb-idstr']/@content"/></xsl:attribute>
	</doc-id>

	<urgency>
		<xsl:attribute name="ed-urg"><xsl:value-of select="meta[@name='NTBPrioritet']/@content"/></xsl:attribute>
	</urgency>

	<date.issue>
		<xsl:attribute name="norm"><xsl:value-of select="$date-iso-norm"/></xsl:attribute>
	</date.issue>

	<ed-msg>
		<xsl:attribute name="info"><xsl:value-of select="meta[@name='NTBBeskjedTilRed']/@content"/></xsl:attribute>
	</ed-msg>

	<du-key>
		<xsl:choose>
			<xsl:when test="meta[@name='NTBMeldingsVersjon']/@content">
				<xsl:attribute name="version"><xsl:value-of select="meta[@name='NTBMeldingsVersjon']/@content + 1"/></xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="version">1</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:attribute name="key">
			<xsl:choose>
				<xsl:when test="starts-with(meta[@name='NTBStikkord']/@content,'HAST-')">
					<xsl:value-of select="substring-after(meta[@name='NTBStikkord']/@content,'HAST-')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="meta[@name='NTBStikkord']/@content"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
	</du-key>

	<doc.copyright>
		<xsl:attribute name="year"><xsl:value-of select="substring(meta[@name='timestamp']/@content, 1, 4)" /></xsl:attribute>
		<xsl:attribute name="holder">
			<xsl:choose>
				<xsl:when test="/w:wordDocument/head/meta[@name='NTBTjeneste' and (@content='NPKTjenesten' or @content='NPKDirektetjenesten')]">
					<xsl:text>NPK</xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>NTB</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:attribute>
	</doc.copyright>

	<key-list>
		<keyword>
			<xsl:attribute name="key"><xsl:value-of select="meta[@name='NTBStikkord']/@content"/></xsl:attribute>
 		</keyword>
	</key-list>
</docdata>

<pubdata>
	<xsl:attribute name="date.publication"><xsl:value-of select="$date-iso" /></xsl:attribute>
	<xsl:attribute name="item-length"><xsl:value-of select="string-length(/w:wordDocument/w:body)"/></xsl:attribute>
	<xsl:attribute name="unit-of-measure">character</xsl:attribute>
</pubdata>

<revision-history>
	<xsl:attribute name="name"><xsl:value-of select="meta[@name='NTBMeldingsSign']/@content"/></xsl:attribute>
</revision-history>


</head>
</xsl:template>

</xsl:stylesheet> 