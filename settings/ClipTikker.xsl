<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="UTF-8" method="xml" indent="yes" standalone="yes"/>

<xsl:param name="filename">
</xsl:param>

<xsl:template match="/">
<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">
	<xsl:apply-templates/>
</nitf>
</xsl:template>

<xsl:template match="head">
<head>
	<xsl:copy-of select="title"/>
	<xsl:apply-templates select="meta"/>
	<xsl:copy-of select="docdata | pubdata | revision-history | tobject"/>
</head>
</xsl:template>

<xsl:template match="meta">
<meta>
	<xsl:attribute name="name">
	<xsl:value-of select="@name"/>
	</xsl:attribute>
	
	<xsl:attribute name="content">
		<xsl:choose>
			<xsl:when test="@name='foldername'">
				<xsl:text>Ut-Tikker</xsl:text>
			</xsl:when>
			<xsl:when test="@name='filename'">
				<xsl:value-of select="$filename"/>
			</xsl:when>
			<xsl:when test="@name='NTBTjeneste'">
				<xsl:text>Tikkertjenesten</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="@content" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:attribute>
</meta>
</xsl:template>

<xsl:template match="body">
<body>
	<xsl:copy-of select="body.head"/>
	<xsl:apply-templates select="body.content"/>
</body>
</xsl:template>

<xsl:template match="body.content">
<body.content>
	<xsl:apply-templates/>
</body.content>	
</xsl:template>

<xsl:template match="p[@lede='true' or @class='lead']">
	<!-- Oppretter P p� nytt -->
	<p>
	
	<!-- Kopierer class properties -->
	<xsl:attribute name="lede">
	<xsl:value-of select="@lede" />
	</xsl:attribute>

	<xsl:attribute name="class">
	<xsl:value-of select="@class" />
	</xsl:attribute>

	<!-- Legger inn innhold -->
	<xsl:value-of select="substring-before(.,'(')"/>
	<xsl:text>(NTB)</xsl:text>
	<xsl:value-of select="substring-after(.,')')"/>
	</p>
</xsl:template>

<xsl:template match="p[string-length(.) &lt; 2]">
	<!-- empty template -->
</xsl:template>

<xsl:template match="p">
	<!-- empty template -->
</xsl:template>

<xsl:template match="hl2">
	<!-- empty template -->
</xsl:template>

<xsl:template match="media">
	<!-- empty template -->
</xsl:template>

<xsl:template match="table">
	<!-- empty template -->
</xsl:template>
</xsl:stylesheet>