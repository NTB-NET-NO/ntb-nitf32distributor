<?xml version="1.0" encoding="iso-8859-1"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml"
	xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint"
	xmlns:o="urn:schemas-microsoft-com:office:office"
>

<xsl:output method="xml" encoding="UTF-8" indent="yes" standalone="yes" omit-xml-declaration="no" />
<!--
	doctype-system="D:\Download\XML\NITF\nitf-3-1.dtd"
	Styleshet for transformasjon fra WordMl (XML-format) med Notabene headers til NITF
	Sist endret Av Roar Vestre 26.09.2003
-->

<!--
<xsl:variable name='infolinje'>
	<xsl:choose>
	<xsl:when test="/w:wordDocument/w:body/wx:sect/w:p[w:pPr/w:pStyle/@w:val='Infolinje' and position() = 1]">
		<xsl:value-of select="/w:wordDocument/w:body/wx:sect/w:p[w:pPr/w:pStyle/@w:val='Infolinje' and position() = 1]"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:choose>
			<xsl:when test="/w:wordDocument/w:body/wx:sect/w:p/w:r/w:t = ''">
				<xsl:value-of select="substring(/w:wordDocument/w:body/wx:sect, 1, 60)" />
			</xsl:when>
			<xsl:when test="string-length(/w:wordDocument/w:body/wx:sect/w:p/w:r/w:t) &lt; 60">
				<xsl:value-of select="/w:wordDocument/w:body/wx:sect/w:p/w:r/w:t" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="substring(/w:wordDocument/w:body/wx:sect/w:p/w:r/w:t, 1, 60)" />
				<xsl:text>.....</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:otherwise>
	</xsl:choose>
</xsl:variable>
-->
<xsl:include href="NyNitfHead.xsl"/>

<xsl:template match="/">
	<xsl:comment>&lt;!DOCTYPE nitf SYSTEM "nitf-3-2.dtd"&gt;</xsl:comment>
	<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">
	<xsl:apply-templates select="/w:wordDocument/head"/>
<!--
	<xsl:call-template name="messageHead"/>
-->	
	<xsl:apply-templates select="/w:wordDocument/w:body"/>
	</nitf>
</xsl:template>

<xsl:template match="w:body">
	<body>
	<body.head>
	<hedline>
		<hl1><xsl:value-of select="$infolinje" /></hl1>
	</hedline>
	
	<xsl:if test="wx:sect/w:p[w:pPr/w:pStyle/@w:val='Byline']">
		<byline>
		<xsl:for-each select="wx:sect/w:p[w:pPr/w:pStyle/@w:val='Byline']/w:r/w:t">
			<xsl:value-of select="."/>
			<xsl:text> </xsl:text>
		</xsl:for-each>
		</byline>
	</xsl:if>
	
	<xsl:choose>
		<xsl:when test="/w:wordDocument/head/meta[@name='NTBTjeneste' and (@content='NPKTjenesten' or @content='NPKDirektetjenesten')]">
			<distributor><org><xsl:text>NPK</xsl:text></org></distributor>
		</xsl:when>
		<xsl:otherwise>
			<distributor><org><xsl:text>NTB</xsl:text></org></distributor>
		</xsl:otherwise>
	</xsl:choose>
	</body.head>
	<body.content>
		<xsl:apply-templates select="wx:sect/w:p | //wx:sub-section/w:p | //w:tbl"/>
<!--		<xsl:apply-templates select="//*[w:p or w:tbl]"/>-->
		<xsl:apply-templates select="/w:wordDocument" mode="media"/>
<!--		<xsl:call-template name="media" />-->
	</body.content>
	
	<body.end>
		<tagline>
			<xsl:for-each select="/w:wordDocument/head/meta[@name='NTBEpostAdresse']">
				<a>
					<xsl:attribute name="href">mailto:<xsl:value-of select="@content"/></xsl:attribute>
					<xsl:value-of select="@content"/>
				</a>
			</xsl:for-each>
		</tagline>
	</body.end>
	
	</body>
</xsl:template>

<!---
<xsl:template match="w:p">
	<xsl:apply-templates select=""/>
</xsl:template>
-->

<!-- default template for unknown p-class names-->
<xsl:template match="w:p">
	<p>
	<xsl:choose>
		<xsl:when test="starts-with(w:pPr/w:pStyle/@w:val,'BrdtekstInnrykk')">
			<xsl:attribute name="class">txt-ind</xsl:attribute>
		</xsl:when>
		<xsl:otherwise>
			<xsl:attribute name="class">txt</xsl:attribute>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:apply-templates/>		
	</p>
</xsl:template>

<xsl:template match="w:r/w:t">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="w:r[w:rPr/w:u]/w:t">
	<em class="underline"><xsl:value-of select="."/></em>
</xsl:template>

<xsl:template match="w:r[w:rPr/w:b]/w:t">
	<em class="bold"><xsl:value-of select="."/></em>
</xsl:template>

<xsl:template match="w:r[w:rPr/w:i]/w:t">
	<em class="italic"><xsl:value-of select="."/></em>
</xsl:template>

<xsl:template match="w:r[w:rPr/w:i and w:rPr/w:b]/w:t">
	<em class="bold"><em class="italic"><xsl:value-of select="."/></em></em>
</xsl:template>

<xsl:template match="w:r/w:br">
	<xsl:text> </xsl:text>
	<br/>
</xsl:template>

<xsl:template match="w:hlink">
	<a>
	<xsl:attribute name="href">
	<xsl:value-of select="@w:dest"/>
	</xsl:attribute>
	<xsl:apply-templates/>	
	</a>
</xsl:template>

<!-- Template to include wrong use of other Ingress-s as Mellomtittel -->
<!-- Template for normal use of Ingress -->
<xsl:template match="w:p[w:pPr/w:pStyle/@w:val='Ingress']">
	<p lede="true" class="lead">
	<!--<xsl:apply-templates select="w:r/w:t"/>-->
	<xsl:apply-templates/>		
	</p>
</xsl:template>

<!--
<xsl:template match="w:p[w:pPr/w:pStyle/@w:val='Ingress' and position() &gt; 2]">
	<hl2>
	<xsl:apply-templates select="w:r/w:t"/>
	</hl2>
</xsl:template>
-->

<!-- Empty Template. Will discard 1st infolinje in text -->
<xsl:template match="w:p[w:pPr/w:pStyle/@w:val='Infolinje' and position() = 1]">
</xsl:template>

<!-- Template to include wrong use of other infolinje-s as Mellomtittel -->
<xsl:template match="w:p[w:pPr/w:pStyle/@w:val='Infolinje' and position() &gt; 1]">
	<hl2>
	<!--<xsl:apply-templates select="w:r/w:t"/>-->
	<xsl:apply-templates/>		
	</hl2>
</xsl:template>

<!-- Empty Template. Will discard byline in text -->
<xsl:template match="w:p[w:pPr/w:pStyle/@w:val='Byline']">
</xsl:template>

<xsl:template match="w:p[w:pPr/w:pStyle/@w:val='Mellomtittel']">
	<hl2>
	<!--<xsl:apply-templates select="w:r/w:t"/>-->
	<xsl:apply-templates/>		
	</hl2>
</xsl:template>

<xsl:template match="w:p[w:pPr/w:pStyle/@w:val='Tabellkode']">
	<p class="table-code"><xsl:apply-templates select="w:r/w:t"/></p>
</xsl:template>

<!-- In case use of Header 1 to 9 style in cut and pastes documents
	into Notabene, convert to Mellomtittel (hl2) -->
<!--<xsl:template match="h1 | h2 | h3 | h4 | h5 | h6">-->
<xsl:template match="w:p[w:pPr/w:pStyle[@w:val='Heading1' or @w:val='Heading2' or @w:val='Heading3' or @w:val='Heading4' or @w:val='Heading5' or @w:val='Heading6']]">
	<hl2><xsl:apply-templates select="w:r/w:t"/></hl2>
</xsl:template>

<!-- Handels Tables -->
<xsl:template match="w:tbl">
	<table>
	<xsl:attribute name="class">
		<xsl:value-of select="count(w:tr[1]/w:tc)"/>
		<xsl:text>-col</xsl:text>
	</xsl:attribute>
	<xsl:apply-templates select="w:tr"/>
	</table>
</xsl:template>

<!-- Table rows -->
<xsl:template match="w:tr">
<tr>
	<xsl:apply-templates select="w:tc"/>
</tr>
</xsl:template>

<xsl:template match="w:tc">
	<td>
	<xsl:if test="w:p/w:pPr/w:jc/@w:val!='' and w:p/w:pPr/w:jc/@w:val!='both'">
	<xsl:attribute name="align"><xsl:value-of select="w:p/w:pPr/w:jc/@w:val"/></xsl:attribute>
	</xsl:if>
	<!--
	<xsl:value-of select="w:p/w:r/w:t"/>
	<xsl:value-of select="w:p"/>
	 -->
    <xsl:apply-templates select="w:p" mode="table"/>
	</td>
</xsl:template>

<xsl:template match="w:p" mode="table">
    <xsl:apply-templates select="w:r"/>
</xsl:template>

<xsl:template match="w:wordDocument" mode="media">
	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '1']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr1']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr1']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr1']/@content"/></xsl:attribute>
		</media-reference>
		
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst1']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '2']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr2']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr2']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr2']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst2']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '3']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr3']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr3']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr3']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst3']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '4']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr4']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr4']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr4']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst4']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '5']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr5']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr5']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr5']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst5']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '6']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr6']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr6']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr6']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst6']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '7']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr7']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr7']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr7']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst7']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '8']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr8']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr8']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr8']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst8']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='NTBBilderAntall']/@content[. &gt;= '9']">
	<media>

		<xsl:choose>
			<xsl:when test="contains(head/meta[@name='NTBBildeNr9']/@content,'.pdf')">
				<xsl:attribute name="media-type">other</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="media-type">image</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:choose>
			<xsl:when test="$tema">
				<xsl:attribute name="class">tema</xsl:attribute>
			</xsl:when>
			<xsl:when test="$kul">
				<xsl:attribute name="class">kultur</xsl:attribute>
			</xsl:when>
		</xsl:choose>

		<media-reference>
			<xsl:choose>
				<xsl:when test="contains(head/meta[@name='NTBBildeNr9']/@content,'.pdf')">
					<xsl:attribute name="mime-type">application/pdf</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="mime-type">image/jpeg</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="source"><xsl:value-of select="head/meta[@name='NTBBildeNr9']/@content"/></xsl:attribute>
		</media-reference>
		<media-caption>
			<xsl:value-of select="head/meta[@name='NTBBildeTekst9']/@content"/>
 		</media-caption>
	</media>
	</xsl:if>

	<xsl:if test="head/meta[@name='ntb-lyd']/@content[. != '']">
		<media media-type="audio">
			<media-reference mime-type="application/x-shockwave-flash">
			<xsl:attribute name="source">http://194.19.39.29/kunde/ntb/flash/<xsl:value-of select="head/meta[@name='ntb-lyd']/@content"/>.swf</xsl:attribute>
			</media-reference>
			<media-reference mime-type="image/gif" source="http://194.19.39.29/kunde/ntb/grafikk/ntb.gif"/>
			<media-reference mime-type="text/javascript" source="http://194.19.39.29/kunde/ntb/flashsound.js"/>
		</media>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>